import discord
from discord.ext.commands import Bot
from discord.ext import commands
import asyncio
import time
import os
import Dice

def getkey():
    return 0

client = discord.Client()



@client.event
async def on_ready():
    print("Torquemada is ready.")
    await client.change_presence(game=discord.Game(name="the trombone"))

@client.event
async def on_message(message):

    if message.content.upper().startswith("ALPHA"):
        args = message.content.split(" ")

        if (args[1] == "roll" or args[1] == "r"):
            modifier_present = False
            dice_parse = args[2].split("d")

            dice_count = dice_parse[0]
            dice_number = dice_parse[1]

            modifier_parse = dice_number.split("+")

            print(modifier_parse)
            
            if (len(modifier_parse) == 1):
                print("No Modifiers Present")
            else:
                modifier_present = True
                modifier = modifier_parse[1]
                print("Modifier Present: " + modifier)


            print("Dice Count = " + dice_count)
            
            if (modifier_present and dice_count == ""):

                if (modifier_parse[0] == "100"):
                    roll = Dice.d100.roll_d100()
                    result = roll + int(modifier)
                    await client.send_message(message.channel, "%s" % "(" + str(roll) + "+" + modifier + ") " + str(result))

                if (modifier_parse[0] == "10"):
                    roll = Dice.d10.roll_d10()
                    result = roll + int(modifier)
                    await client.send_message(message.channel, "%s" % "(" + str(roll) + "+" + modifier + ") " + str(result))


                if (modifier_parse[0] == "5"):
                    roll = Dice.d5.roll_d5()
                    result = roll + int(modifier)
                    await client.send_message(message.channel, "%s" % "(" + str(roll) + "+" + modifier + ") " + str(result))
            elif (modifier_present and not dice_count == ""):
                
                if (modifier_parse[0] == "100"):
                    result, rolls = Dice.d100.roll_multiple_d100(int(dice_count))
                    result = result + int(modifier)
                    await client.send_message(message.channel, "%s" % "(" + rolls + "+" + modifier + ") " + str(result))
                if (modifier_parse[0] == "10"):
                    result, rolls = Dice.d10.roll_multiple_d10(int(dice_count))
                    result = result + int(modifier)
                    await client.send_message(message.channel, "%s" % "(" + rolls + "+" + modifier + ") " + str(result))
                if (modifier_parse[0] == "5"):
                    result, rolls = Dice.d5.roll_multiple_d5(int(dice_count))
                    result = result + int(modifier)
                    await client.send_message(message.channel, "%s" % "(" + rolls + "+" + modifier + ") " + str(result))
                    
                
                
                
            if (dice_parse[0] == ""):
                print(args[2])
                if (args[2] == "d100"):
                    await client.send_message(message.channel, "%s" % str(Dice.d100.roll_d100()))
                if (args[2] == "d10"):
                    await client.send_message(message.channel, "%s" % str(Dice.d10.roll_d10()))
                if (args[2] == "d5"):
                    await client.send_message(message.channel, "%s" % str(Dice.d5.roll_d5()))
            else:
                if (dice_number == "10"):
                    result, rolls = Dice.d10.roll_multiple_d10(int(dice_count))
                    await client.send_message(message.channel, "%s" % "(" + rolls + ") " + str(result))
                if (dice_number == "100"):
                    result, rolls = Dice.d100.roll_multiple_d100(int(dice_count))
                    await client.send_message(message.channel, "%s" % "(" + rolls + ") " + str(result))
                if (dice_number == "5"):
                    result, rolls = Dice.d5.roll_multiple_d5(int(dice_count))
                    await client.send_message(message.channel, "%s" % "(" + rolls + ") " + str(result))


client.run("NDIzNjcyMzQ2MjQ3ODIzMzgw.DYtzXg.7nTvdr2a1LJbucmNzX8uDRmJkz0")
