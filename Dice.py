import os
import random

class d100:
    def __init__(self):
        os.random(os.urandom())

    def roll_d100():
        return random.randint(1,100)

    def roll_multiple_d100(count):
        sum = 0
        rolls = ""
        for i in range(int(count)):
            roll = random.randint(1,100)

            sum = sum + roll

            if (i != int(count) - 1):
                rolls = rolls + str(roll) + "+"
            else:
                rolls = rolls + str(roll)
        return sum, rolls

class d10:
    def __init__(self):
        os.random(os.urandom())

    def roll_d10():
        return random.randint(1,10)

    def roll_multiple_d10(count):
        sum = 0
        rolls = ""
        for i in range(int(count)):
            roll = random.randint(1,10)

            sum = sum + roll

            if (i != int(count) - 1):
                rolls = rolls + str(roll) + "+"
            else:
                rolls = rolls + str(roll)
        return sum, rolls

class d5:
    def __init__(self):
        os.random(os.urandom())
        
    def roll_d5():
        return random.randint(1,5)

    def roll_multiple_d5(count):
        sum = 0
        rolls = ""
        for i in range(int(count)):
            roll = random.randint(1,5)

            sum = sum + roll

            if (i != int(count) - 1):
                rolls = rolls + str(roll) + "+"
            else:
                rolls = rolls + str(roll)
        return sum, rolls
