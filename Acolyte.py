

class Acolyte:

    def __init__(self, weapon_skill, ballistic_skill, strength, toughness,
                 agility, intelligence, willpower, fellowship, influence):

        self.ws = weapon_skill
        self.bs = ballistic_skill
        self.str = strength
        self.tough = toughness
        self.agil = agility
        self.int = intelligence
        self.fell = fellowship
        self.inf = influence

    def test_ws(self, roll):

        if (self.ws >= roll):
            print("PASS")
        else:
            print("FAIL")
        return 0;
        
